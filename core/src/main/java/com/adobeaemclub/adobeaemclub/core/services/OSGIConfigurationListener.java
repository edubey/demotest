package com.adobeaemclub.adobeaemclub.core.services;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationEvent;
import org.osgi.service.cm.ConfigurationListener;
import org.osgi.service.cm.ConfigurationAdmin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Dictionary;

/**
 * Service will act as a listener for any configuration change and will show update property.
 * @author praveen
 *
 */
@Component
@Service
public class OSGIConfigurationListener implements ConfigurationListener {

	public static final Logger log = LoggerFactory.getLogger(OSGIConfigurationListener.class);

	public final static String OSGI_SERVICE_EMAIl_CHANNEL = "com.day.cq.wcm.notification.email.impl.EmailChannel";

	public final static String EMAIL_FROM = "email.from";

	@Reference
	ConfigurationAdmin configurationAdmin;

	@Override
	public void configurationEvent(ConfigurationEvent event) {
		try {
			if (event.getPid().equals(OSGI_SERVICE_EMAIl_CHANNEL)) {
				readUpdatedConfig();
			}
		} catch (Exception exp) {
			log.info("Exception found while listening for change on {}, {} ", OSGI_SERVICE_EMAIl_CHANNEL, exp.getMessage());
		}
	}

	/**
	 * Method will be executed once there is any change in the property of 
	 * <code> com.day.cq.widget.impl.HtmlLibraryManagerImpl</code> service
	 */
	public void readUpdatedConfig() {
		try {
			
			Configuration readConfig = configurationAdmin.getConfiguration(OSGI_SERVICE_EMAIl_CHANNEL);
			Dictionary<String, String> properties = readConfig.getProperties();
			log.info("Updated property is " + properties.get(EMAIL_FROM));

		} catch (IOException e) {
			log.info("Exception while reading property of {}, {}", EMAIL_FROM, e.getMessage());
		}

	}

}