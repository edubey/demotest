package com.adobeaemclub.adobeaemclub.core.services;

import javax.script.Bindings;
import org.apache.sling.api.resource.Resource;
import io.sightly.java.api.Use;

public class DeveloperProfile implements Use {

	private String developer;

	@Override
	public void init(Bindings bindings) {
		// all standard objects/binding are available
		Resource resource = (Resource) bindings.get("resource");

		// parameters are passed as bindings
		String developerName = (String) bindings.get("devName");
		String tool = (String) bindings.get("tool");
		String aboutMe = (String) bindings.get("about");

		developer = "Our developer " + developerName.toUpperCase()
				+ " works on " + tool.toUpperCase()
				+ ", Here's whats he says about " + "himself\n\"" + aboutMe
				+ "\"";

	}

	public String getProfile() {
		return developer;
	}
}